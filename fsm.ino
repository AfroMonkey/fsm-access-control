#include "StateMachineLib.h"
#include "SoftwareSerial.h"
#include "DFRobotDFPlayerMini.h"

const unsigned RX_PIN = 10;
const unsigned TX_PIN = 11;

const unsigned InvalidAudio = 1;
const unsigned MissingStepAudio = 2;
const unsigned DoneAudio = 3;

SoftwareSerial dfpSerial(RX_PIN, TX_PIN); // RX, TX
DFRobotDFPlayerMini dfplayer;

enum State {
  Start = 0,
  AwaitTemp = 1,
  AwaitGel = 2,
  Approved = 3,
  Done = 4,
  MissingStep = 5,
  Invalid = 6,
};

enum Input {
  Entrance = 0,
  Exit = 1,
  TemperatureOk = 2,
  TemperatureBad = 3,
  GelApplication = 4,
  Unknown = -1,
};

enum Pin {
  EntrancePin = 2,
  ExitPin = 3,
  TemperatureOkPin = 4,
  TemperatureBadPin = 5,
  GelApplicationPin = 6,
  DonePin = 7,
  MissingStepPin = 8,
  InvalidPin = 9,
};

StateMachine stateMachine(7, 10);
Input input;

void setupTransitions() {
  // State Start transations
  stateMachine.AddTransition(Start, AwaitTemp, [](){ return input == Entrance; });
  // State AwaitTemp transations
  stateMachine.AddTransition(AwaitTemp, AwaitGel, [](){ return input == TemperatureOk; });
  stateMachine.AddTransition(AwaitTemp, Invalid, [](){ return input == TemperatureBad; });
  stateMachine.AddTransition(AwaitTemp, MissingStep, [](){ return input == Exit; });
  // State AwaitGel transations
  stateMachine.AddTransition(AwaitGel, Approved, [](){ return input == GelApplication; });
  stateMachine.AddTransition(AwaitGel, MissingStep, [](){ return input == Exit; });
  // State Approved transations
  stateMachine.AddTransition(Approved, Done, [](){ return input == Exit; });
  // State Final transations
  stateMachine.AddTransition(Done, Start, [](){ return true; });
  stateMachine.AddTransition(MissingStep, Start, [](){ return true; });
  stateMachine.AddTransition(Invalid, Start, [](){ return true; });
}

void enteringDone() {
  digitalWrite(DonePin, HIGH);
  dfplayer.play(DoneAudio);
  Serial.println("enteringDone");
  delay(2000);
  digitalWrite(DonePin, LOW);
}

void enteringMissingStep() {
  digitalWrite(MissingStepPin, HIGH);
  dfplayer.play(MissingStepAudio);
  Serial.println("enteringMissingStep");
  delay(5000);
  digitalWrite(MissingStepPin, LOW);
}

void enteringInvalid() {
  digitalWrite(InvalidPin, HIGH);
  dfplayer.play(InvalidAudio);
  Serial.println("enteringInvalid");
  delay(5000);
  digitalWrite(InvalidPin, LOW);
}

void setupEnterings() {
  stateMachine.SetOnEntering(Start, [](){ Serial.println("Entering Start"); });
  stateMachine.SetOnEntering(AwaitTemp, [](){ Serial.println("Entering AwaitTemp"); });
  stateMachine.SetOnEntering(AwaitGel, [](){ Serial.println("Entering AwaitGel"); });
  stateMachine.SetOnEntering(Approved, [](){ Serial.println("Entering Approved"); });
  stateMachine.SetOnEntering(Done, enteringDone);
  stateMachine.SetOnEntering(MissingStep, enteringMissingStep);
  stateMachine.SetOnEntering(Invalid, enteringInvalid);
}

void setupStateMachine() {
  setupTransitions();
  setupEnterings();
  // setupLeavings();
  stateMachine.SetState(Start, false, true);
}

Input readInput() {
  if (digitalRead(EntrancePin) == HIGH) return Entrance;
  if (digitalRead(ExitPin) == HIGH) return Exit;
  if (digitalRead(TemperatureOkPin) == HIGH) return TemperatureOk;
  if (digitalRead(TemperatureBadPin) == HIGH) return TemperatureBad;
  if (digitalRead(GelApplicationPin) == HIGH) return GelApplication;
  return Unknown;
}

void setupIO() {
  // Inputs
  pinMode(EntrancePin, INPUT);
  pinMode(ExitPin, INPUT);
  pinMode(TemperatureOkPin, INPUT);
  pinMode(TemperatureBadPin, INPUT);
  pinMode(GelApplicationPin, INPUT);
  // Outputs
  pinMode(DonePin, OUTPUT);
  pinMode(MissingStepPin, OUTPUT);
  pinMode(InvalidPin, OUTPUT);
}

void setupDFP() {
  if (!dfplayer.begin(dfpSerial)) {
    Serial.println("Unable to begin:");
    Serial.println("1.Please recheck the connection!");
    Serial.println("2.Please insert the SD card!");
    while(true);
  }
  dfplayer.volume(30);
}

void setup() {
  Serial.begin(9600);
  dfpSerial.begin(9600);

  setupDFP();
  setupIO();
  setupStateMachine();
}

void loop() {
  input = readInput();
  stateMachine.Update();
}
